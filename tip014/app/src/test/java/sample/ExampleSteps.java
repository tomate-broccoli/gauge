package sample;

import com.thoughtworks.gauge.Step;
import com.thoughtworks.gauge.Table;
import com.thoughtworks.gauge.TableRow;

import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;

public class ExampleSteps {

    @Step("step: read csv. <table:data.csv>")
    public void step1(Table arg0){
        // throw new UnsupportedOperationException("Provide custom implementation");
        for(TableRow row : arg0.getTableRows()){
            String col1 = row.getCell("head1");
            String col2 = row.getCell("head2");
            System.out.printf("** step1: col1:[%s], col2:[%s].\n", col1.trim(), col2.trim());
        }
    }

    @Step("step: read table. <table>")
    public void step2(Table arg0){
        // throw new UnsupportedOperationException("Provide custom implementation");
        for(TableRow row : arg0.getTableRows()){
            String col1 = row.getCell("head1");
            String col2 = row.getCell("head2");
            System.out.printf("** step2: col1:[%s], col2:[%s].\n", col1.trim(), col2.trim());
        }
    }

    @Step("read csvs. <table:data01.csv> <table:data02.csv>")
    public void step3(Table arg0, Table arg1){
        // throw new UnsupportedOperationException("Provide custom implementation");
        for(TableRow row : arg0.getTableRows()){
            String col1 = row.getCell("head1");
            String col2 = row.getCell("head2");
            System.out.printf("** step3: arg0: col1:[%s], col2:[%s].\n", col1.trim(), col2.trim());
        }
        for(TableRow row : arg1.getTableRows()){
            String col1 = row.getCell("head1");
            String col2 = row.getCell("head2");
            System.out.printf("** step3: arg1: col1:[%s], col2:[%s].\n", col1.trim(), col2.trim());
        }
    }

}
