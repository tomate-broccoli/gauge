# test01.spec

  $ gradle clean gauge -PspecsDir=specs/test01.spec --info

  | foo | bar |
  |-----|-----|
  | FOO | BAR |
 
## scenario 1.
* step: read table.
  | head1 | head2 |
  |-------|-------|
  | 1     | <foo> |
  | 2     | <bar> |

## scenario 2.
* step: read csv. <table:data03.csv>
