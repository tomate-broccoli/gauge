# test001.spec

  $ gradle clean gauge -PspecsDir=specs/test001.spec --info
 
  table in spec area.

  | col1 | col2 | col3 |
  | 1    | abc  | ABC  |
  | 2    | xyz  | XYZ  |

## scenario 1.

* step 1. col1: <col1>, col2: <col2>

* step2 . 1 row.
  | head1  | head2 |
  | <col1> | <col2> |

* step3 . 2 row.
  | head1  | head2 |
  | <col1> | <col2> |
  | <col1> | <col3> |
