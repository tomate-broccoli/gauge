# test005.spec

  $ gradle clean gauge -PspecsDir=specs/test005.spec --info

## scenario 1.

* read tables.

    Add the following missing implementations to fix `Step implementation not found` errors.
        @Step("read tables. <table>")
        public void readTables.(Object arg0){
                throw new UnsupportedOperationException("Provide custom implementation");
        }

  | head1 | head2  |
  | 1-1   | abc    |
  | 1-2   | zyz    |

  | head1 | head2  |
  | 2-1   | ABC    |
  | 2-2   | XYZ    |
