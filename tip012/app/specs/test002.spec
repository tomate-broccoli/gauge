# test002.spec

  $ gradle clean gauge -PspecsDir=specs/test002.spec --info
 
  table in spec area.

  | col1 | col2 | col3 |
  | 1    | abc  | ABC  |
  | 2    | xyz  | XYZ  |

## scenario 1.

* step3 . 2 row.
  | head1 | head2  |
  | 1-1   | <col1> |
  | 1-2   | <col1> | 

* step4 . 3 row.
  | head1 | head2  | 
  | 2-1   | <col1> | 
  | 2-2   | <col2> | 
  | 2-3   | <col3> | 
