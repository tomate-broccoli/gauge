package sample;

import com.thoughtworks.gauge.Step;
import com.thoughtworks.gauge.Table;
import com.thoughtworks.gauge.TableRow;

// import java.util.HashSet;
// import static org.assertj.core.api.Assertions.assertThat;

public class Tip012Steps {

    @Step("step 1. col1: <col1>, col2: <col2>")
    public void step1(int arg0, String arg1){
        // throw new UnsupportedOperationException("Provide custom implementation");
        System.out.printf("\n** step1: col1:[%d], col2:[%s].\n", arg0, arg1.trim());
    }

    @Step("step2 . 1 row. <table>")
    public void step2(Table arg0){
        // throw new UnsupportedOperationException("Provide custom implementation");
        for(TableRow row : arg0.getTableRows()){
            String col1 = row.getCell("head1");
            String col2 = row.getCell("head2");
            System.out.printf("** step2: col1:[%s], col2:[%s].\n", col1, col2);
        }
    }

    @Step("step3 . 2 row. <table>")
    public void step3(Table arg0){
        // throw new UnsupportedOperationException("Provide custom implementation");
        for(TableRow row : arg0.getTableRows()){
            String col1 = row.getCell("head1");
            String col2 = row.getCell("head2");
            System.out.printf("** step3: col1:[%s], col2:[%s].\n", col1.trim(), col2.trim());
        }
    }

    @Step("step4 . 3 row. <table>")
    public void step4(Table arg0){
        // throw new UnsupportedOperationException("Provide custom implementation");
        for(TableRow row : arg0.getTableRows()){
            String col1 = row.getCell("head1");
            String col2 = row.getCell("head2");
            System.out.printf("** step4: col1:[%s], col2:[%s].\n", col1.trim(), col2.trim());
        }
    }

    @Step("table and csv. <table:data01.csv> <table>")
    public void step5(Table arg0, Table arg1){
        // throw new UnsupportedOperationException("Provide custom implementation");
        for(TableRow row : arg0.getTableRows()){
            String col1 = row.getCell("head1");
            String col2 = row.getCell("head2");
            System.out.printf("** step5: csv: col1:[%s], col2:[%s].\n", col1.trim(), col2.trim());
        }
        for(TableRow row : arg1.getTableRows()){
            String col1 = row.getCell("head1");
            String col2 = row.getCell("head2");
            System.out.printf("** step5: table: col1:[%s], col2:[%s].\n", col1.trim(), col2.trim());
        }
    }

}
