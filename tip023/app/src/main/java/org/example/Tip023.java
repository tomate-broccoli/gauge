package org.example;

import com.thoughtworks.gauge.BeforeScenario;
import com.thoughtworks.gauge.BeforeSpec;
import com.thoughtworks.gauge.BeforeSuite;
import com.thoughtworks.gauge.Step;

public class Tip023 {

    @BeforeSuite
    public void suite(){
        System.out.println("** Tip0023.suite called.");
    }

    @BeforeScenario
    public void scenario(){
        System.out.println("** Tip0023.scenario called.");
    }

    @BeforeSpec
    public void spec(){
        System.out.println("** Tip0023.spec called.");
    }

    @Step("STEP 1:")
    public void step1(){
        // throw new UnsupportedOperationException("Provide custom implementation");
        System.out.println("** Tip0023.step1 called.");
    }

    @Step("STEP 2:")
    public void step2(){
        // throw new UnsupportedOperationException("Provide custom implementation");
        System.out.println("** Tip0023.step2 called.");
    } 

}
