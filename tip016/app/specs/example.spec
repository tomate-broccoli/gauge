# example.spec

  $ gradle clean gauge -PspecsDir=specs/example.spec --info

  | foo | bar |
  |-----|-----|
  | FOO | BAR |
 
## scenario 1.
* concept: <foo> read csv. <table:data01.csv>
* concept: <bar> read csv. <table:data02.csv>
