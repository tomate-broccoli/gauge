# example.spec

  $ gradle clean gauge -PspecsDir=specs/test001.spec --info

  | foo | bar |
  |-----|-----|
  | FOO | BAR |
 
## scenario 1.
* concept: read csv and table. <table:data01.csv>
  | head1 | head2 |
  |-------|-------|
  | 1-1   | <foo> |
  | 1-2   | <bar> |
