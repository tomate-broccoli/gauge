package sample;

import com.thoughtworks.gauge.Step;
import com.thoughtworks.gauge.Table;
import com.thoughtworks.gauge.TableRow;

import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;

public class ExampleSteps {

    @Step("step: <key> read csv. <data_csv>")
    public void step1(String key, Table arg0){
        // throw new UnsupportedOperationException("Provide custom implementation");
        for(TableRow row : arg0.getTableRows()){
            String col1 = row.getCell("head1");
            String col2 = row.getCell("head2");
            System.out.printf("** step1: key:[%s], col1:[%s], col2:[%s].\n", key, col1.trim(), col2.trim());
        }
    }

    @Step("step: read csv and table. <data_csv> <table>")
    public void step2(Table arg0, Table arg1){
        // throw new UnsupportedOperationException("Provide custom implementation");
        for(TableRow row : arg0.getTableRows()){
            String col1 = row.getCell("head1");
            String col2 = row.getCell("head2");
            System.out.printf("** step2: csv: col1:[%s], col2:[%s].\n", col1.trim(), col2.trim());
        }
        for(TableRow row : arg1.getTableRows()){
            String col1 = row.getCell("head1");
            String col2 = row.getCell("head2");
            System.out.printf("** step2: table: col1:[%s], col2:[%s].\n", col1.trim(), col2.trim());
        }
    }

}
